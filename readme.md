

# Installation

## Install the required python packages
    ~/www/codeart$ sudo apt-get install python-virtualenv python-dev 

## Create a new Python virtual environment and install Flask plus all the required packages:
    ~/www/codeart$ virtualenv venv
    ~/www/codeart$ source venv/bin/activate
    ~/www/codeart$ cd config && pip install -r requirements.txt

## Apache2 server
If we'll want to run our application with Apache, as in the real server, we need to install it along
with the WSGI module, which provides integration with Python applications: 
 
    ~/www/codeart$ sudo apt-get install apache2 libapache2-mod-wsgi


# Running the application
We can run the application either using the built-in Flask web server, which will be enough for development purposes,
or through the Apache2 Web server.


## Running the application locally for development purposes
Running the built-in server is as easy as activating the previously-installed virtual environment and bootstraping our `run` script:

    ~/www/codeart$ source venv/bin/activate
    ~/www/codeart$ python run.py
    

## Running the application on the codeart server (Apache2)
The real `codeart` server URLS are:

* [http://codeart-ai.s.upf.edu/](http://codeart-ai.s.upf.edu/) (Development Server, accessible only from within the UPF network)
* [http://codeart.ai.upf.edu/](http://codeart.ai.upf.edu/) (Production Server)

### SSH'ing into the server
You can login into the server with:

    $ ssh codeart-ai.s.upf.edu

The development *and* the production environments are mapped, respectively, into `/usr/share/sites/codeart-dev` and
`/usr/share/sites/codeart-prod`. In order to update the code, the safest thing is to pull the changes from the repository and 
then restart the apache server. For instance, to update the development server:

    /usr/share/sites/codeart-dev$ git pull
    /usr/share/sites/codeart-dev$ sudo service apache2 restart


<!---
In order to channel the HTTP requests through Apache, we need to perform a few configuration steps:

    ~/www/codeart$ cd /etc/apache2/sites-available
    /etc/apache2/sites-available$ sudo ln -s /usr/share/sites/codeart-dev/codeart-dev.conf .
    /etc/apache2/sites-available$ sudo a2ensite codeart-dev
    /etc/apache2/sites-available$ sudo service apache2 restart
-->


# Project Structure
The project follows a typical Flask project structure

    ~/www/codeart/web$ tree -L 1
    .
    ├── __init__.py
    ├── lessons
    ├── static
    ├── templates
    ├── utils.py


`__init__.py` contains the application entry point. Basically, it declares the URL paths for every "lesson controller"
(which technically is a [Flask Blueprint](http://flask.pocoo.org/docs/latest/blueprints/)),
 along with a couple other minor details.  The `lessons` directory contains the controllers for each lesson, whereas the
  `static` directory contains all sorts of static files (css, js, images), and the `templates` directory contains the
  Jinja templates.
  
This means that every lesson is structured as follows. Lesson 1, called "sequencies", has its main controller on
`lessons/sequencies/__init__.py`, where all the URLs related to the lesson (including Blockly iframe URLs) are declared.
Each lesson has an "index" template which is the introductory page to the lesson
(in this case, `templates/lessons/sequencies/index.html`), plus a number of actual exercises (in this case, 
`templates/lessons/sequencies/door1.html`, `templates/lessons/sequencies/door2.html`, etc.).
The composition of the menu is automatically generated from each of the `@menu.register_menu` decorators on the lesson
controller.
Besides, the static files related to each lesson are in the `static` directory, e.g. in this case in
`static/lessons/sequencies`

 
