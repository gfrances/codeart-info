from flask import Blueprint, url_for, render_template
import os


def list_directories(path):
    """ Returns the immediate subdirectories in the given path """
    return [name for name in os.listdir(path) if os.path.isdir(os.path.join(path, name))]


class Lesson(object):
    def __init__(self, name, import_name):
        self.name = name
        self.import_name = import_name
        self.url = lambda s: url_for("static", filename="lessons/{}/{}".format(self.name, s))

    def generate_blueprint(self):
        return Blueprint(self.name, self.import_name)

    def render(self, tpl, **kwargs):
        return render_template('lessons/{}/{}'.format(self.name, tpl), _url=self.url, **kwargs)


