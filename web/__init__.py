
# This is a small hack to allow the use of utf-8 characters in source code.
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

from flask import Flask, render_template
from flask_bootstrap import Bootstrap
from flask.ext.assets import Environment, Bundle
from web.utils import list_directories
from flask.ext import menu

app = Flask(__name__)
app.config.from_object('config')
Bootstrap(app)
menu.Menu(app=app)

assets = Environment(app)

app.config['ASSETS_DEBUG'] = True

assets.register('js_base', Bundle('js/vendor/jquery-1.11.2.min.js', 'js/vendor/modernizr-2.8.3-respond-1.4.2.min.js',
                                  'js/main.js',
                                  filters='jsmin', output='gen/base.js'))

assets.register('css_base', Bundle('css/normalize.min.css', 'css/main.css', 'css/codeart.css', 'css/simple-sidebar.css',
                                   output='gen/base.css'))


assets.register('blockly_js_base', Bundle('js/vendor/jquery-1.11.2.min.js',
                                          'lib/blockly/blockly_compressed.js',
                                          'lib/blockly/javascript_compressed.js',
                                          'lib/blockly/blocks_compressed.js',
                                          'lib/blockly/msg/js/en.js',
                                          filters='jsmin', output='gen/base.js'))

def register_lessons(app):
    """ Register all the lesson blueprints """
    # TODO - Dynamic imports:
    # http://librelist.com/browser/flask/2012/10/6/import-blueprints-dynamically/#29eefbeda9cf6ddfccde549d4718bcc7
    # def parse_lessons():
    #     lesson_dir = os.path.dirname(os.path.realpath(__file__)) + '/lessons'
    #     return {l: lesson_dir + '/' + l for l in list_directories(lesson_dir)}
    #
    # LESSONS = parse_lessons()
    from lessons.intro import blueprint as intro
    app.register_blueprint(intro, url_prefix='/lessons/intro')

    from lessons.sequencies import blueprint as sequencies
    app.register_blueprint(sequencies, url_prefix='/lessons/sequencies')

    from lessons.physics import blueprint as physics
    app.register_blueprint(physics, url_prefix='/lessons/physics')

    from lessons.events import blueprint as events
    app.register_blueprint(events, url_prefix='/lessons/events')
    
    from lessons.variables import blueprint as variables
    app.register_blueprint(variables, url_prefix='/lessons/variables')

    from lessons.loops import blueprint as loops
    app.register_blueprint(loops, url_prefix='/lessons/loops')
    
    from lessons.conditionals import blueprint as conditionals
    app.register_blueprint(conditionals, url_prefix='/lessons/conditionals')
    
    from lessons.functions import blueprint as functions
    app.register_blueprint(functions, url_prefix='/lessons/functions')
    
    from lessons.cplusplus import blueprint as cplusplus
    app.register_blueprint(cplusplus, url_prefix='/lessons/cplusplus')


register_lessons(app)

@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


@app.route('/')
def homepage():
    return render_template('home.html')



@app.route('/about')
def about():
    return render_template('about.html')

# print(app.url_map)
