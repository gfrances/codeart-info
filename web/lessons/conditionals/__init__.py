# coding=utf-8
from flask import url_for
from web.utils import Lesson
from flask.ext import menu

lesson = Lesson('conditionals', __name__)
blueprint = lesson.generate_blueprint()

@blueprint.route('/')
@menu.register_menu(blueprint, '.conditionals', '6. Condicionals', order=6)
def index():
    return lesson.render('index.html')
