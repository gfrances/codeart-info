# coding=utf-8
from flask import url_for
from web.utils import Lesson
from flask.ext import menu

lesson = Lesson('physics', __name__)
blueprint = lesson.generate_blueprint()


@blueprint.route('/')
@menu.register_menu(blueprint, '.physics', '2. Valors', order=2)
def index():
    return lesson.render('index.html')


@blueprint.route('/physics1')
@menu.register_menu(blueprint, '.physics.porta1', 'Porta 1', order=1)
def physics1():
    return lesson.render('door1.html',
                         iframe_target=url_for('physics.canvas1'))

@blueprint.route('/canvas1')
def canvas1():
    return lesson.render('canvas1.html')

@blueprint.route('/physics2')
@menu.register_menu(blueprint, '.physics.porta2', 'Porta 2', order=2)
def physics2():
    return lesson.render('door2.html',
                         iframe_target=url_for('physics.canvas2'))

@blueprint.route('/canvas2')
def canvas2():
    return lesson.render('canvas2.html')

@blueprint.route('/physics3')
@menu.register_menu(blueprint, '.physics.porta3', 'Porta 3', order=3)
def physics3():
    return lesson.render('door3.html',
                         iframe_target=url_for('physics.canvas3'))

@blueprint.route('/canvas3')
def canvas3():
    return lesson.render('canvas3.html')


