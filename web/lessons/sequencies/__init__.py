# coding=utf-8
from flask import url_for
from web.utils import Lesson
from flask.ext import menu

lesson = Lesson('sequencies', __name__)
blueprint = lesson.generate_blueprint()


@blueprint.route('/')
@menu.register_menu(blueprint, '.sequencies', '1. Seqüències', order=1)
def index():
    return lesson.render('index.html', doors=[str(i) for i in range(1, 10)])


@blueprint.route('/door1')
@menu.register_menu(blueprint, '.sequencies.porta1', 'Porta 1', order=1)
def door1():
    return lesson.render('door1.html', doors=[str(i) for i in range(1, 10)],
                         iframe_target=url_for('sequencies.canvas1'))

@blueprint.route('/canvas1')
def canvas1():
    return lesson.render('canvas1.html')


@blueprint.route('/door2')
@menu.register_menu(blueprint, '.sequencies.porta2', 'Porta 2', order=2)
def door2():
    return lesson.render('door2.html', doors=[str(i) for i in range(1, 10)],
                         iframe_target=url_for('sequencies.canvas2'))

@blueprint.route('/canvas2')
def canvas2():
    return lesson.render('canvas2.html')


@blueprint.route('/door3')
@menu.register_menu(blueprint, '.sequencies.porta3', 'Porta 3', order=3)
def door3():
    return lesson.render('door3.html', doors=[str(i) for i in range(1, 10)],
                         iframe_target=url_for('sequencies.canvas3'))

@blueprint.route('/canvas3')
def canvas3():
    return lesson.render('canvas3.html')


@blueprint.route('/door4')
@menu.register_menu(blueprint, '.sequencies.porta4', 'Porta 4', order=4)
def door4():
    return lesson.render('door4.html', doors=[str(i) for i in range(1, 10)],
                         iframe_target=url_for('sequencies.canvas4'))

@blueprint.route('/canvas4')
def canvas4():
    return lesson.render('canvas4.html')


@blueprint.route('/door5')
@menu.register_menu(blueprint, '.sequencies.porta5', 'Porta 5', order=5)
def door5():
    return lesson.render('door5.html', doors=[str(i) for i in range(1, 10)],
                         iframe_target=url_for('sequencies.canvas5'))

@blueprint.route('/canvas5')
def canvas5():
    return lesson.render('canvas5.html')


@blueprint.route('/door6')
@menu.register_menu(blueprint, '.sequencies.porta6', 'Porta 6', order=6)
def door6():
    return lesson.render('door6.html', doors=[str(i) for i in range(1, 10)],
                         iframe_target=url_for('sequencies.canvas6'))

@blueprint.route('/canvas6')
def canvas6():
    return lesson.render('canvas6.html')


@blueprint.route('/door7')
@menu.register_menu(blueprint, '.sequencies.porta7', 'Porta 7', order=7)
def door7():
    return lesson.render('door7.html', doors=[str(i) for i in range(1, 10)],
                         iframe_target=url_for('sequencies.canvas7'))

@blueprint.route('/canvas7')
def canvas7():
    return lesson.render('canvas7.html')


@blueprint.route('/door8')
@menu.register_menu(blueprint, '.sequencies.porta8', 'Porta 8', order=8)
def door8():
    return lesson.render('door8.html', doors=[str(i) for i in range(1, 10)],
                         iframe_target=url_for('sequencies.canvas8'))

@blueprint.route('/canvas8')
def canvas8():
    return lesson.render('canvas8.html')
