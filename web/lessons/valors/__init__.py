

from web.utils import Lesson
from flask.ext import menu

lesson = Lesson('valors', __name__)
blueprint = lesson.generate_blueprint()


@blueprint.route('/')
@menu.register_menu(blueprint, '.valors', '2. Valors', order=2)
def index():
    return lesson.render('index.html')


@blueprint.route('/test1')
@menu.register_menu(blueprint, '.valors.test1', 'Test1', order=1)
def test1():
    return lesson.render('index.html')

@blueprint.route('/test2')
@menu.register_menu(blueprint, '.valors.test2', 'Test2', order=2)
def test2():
    return lesson.render('index.html')
