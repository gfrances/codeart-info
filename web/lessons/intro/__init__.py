# coding=utf-8
from web.utils import Lesson
from flask.ext import menu

lesson = Lesson('intro', __name__)
blueprint = lesson.generate_blueprint()


@blueprint.route('/')
@menu.register_menu(blueprint, '.introduccio', 'Introducció', order=0)
def index():
    return lesson.render('index.html')
