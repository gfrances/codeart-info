# coding=utf-8
from flask import url_for
from web.utils import Lesson
from flask.ext import menu

lesson = Lesson('cplusplus', __name__)
blueprint = lesson.generate_blueprint()

@blueprint.route('/')
@menu.register_menu(blueprint, '.cplusplus', '8. C++', order=8)
def index():
    return lesson.render('index.html')
