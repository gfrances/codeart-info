# coding=utf-8
from flask import url_for
from web.utils import Lesson
from flask.ext import menu

lesson = Lesson('loops', __name__)
blueprint = lesson.generate_blueprint()

@blueprint.route('/')
@menu.register_menu(blueprint, '.loops', '5. Bucles', order=5)
def index():
    return lesson.render('index.html')
