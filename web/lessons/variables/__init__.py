# coding=utf-8
from flask import url_for
from web.utils import Lesson
from flask.ext import menu

lesson = Lesson('variables', __name__)
blueprint = lesson.generate_blueprint()

@blueprint.route('/')
@menu.register_menu(blueprint, '.variables', '4. Variables', order=4)
def index():
    return lesson.render('index.html')
