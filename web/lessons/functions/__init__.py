# coding=utf-8
from flask import url_for
from web.utils import Lesson
from flask.ext import menu

lesson = Lesson('functions', __name__)
blueprint = lesson.generate_blueprint()

@blueprint.route('/')
@menu.register_menu(blueprint, '.functions', '7. Funcions', order=7)
def index():
    return lesson.render('index.html')
