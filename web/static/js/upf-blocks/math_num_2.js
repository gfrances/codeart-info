Blockly.Blocks['math_num_2'] = {
  init: function() {
    this.setColour(160);
	this.setOutput(true, 'Number');
	this.appendDummyInput()
		.appendField("2");
    this.setTooltip('');
  }
};

Blockly.JavaScript['math_num_2'] = function(block) {
  var code = 'game.useNum(2)';
  return [code, Blockly.JavaScript.ORDER_ADDITION];
};
