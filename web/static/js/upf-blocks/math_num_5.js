Blockly.Blocks['math_num_5'] = {
  init: function() {
    this.setColour(160);
	this.setOutput(true, 'Number');
	this.appendDummyInput()
		.appendField("5");
    this.setTooltip('');
  }
};

Blockly.JavaScript['math_num_5'] = function(block) {
  var code = 'game.useNum(5)';
  return [code, Blockly.JavaScript.ORDER_ADDITION];
};
