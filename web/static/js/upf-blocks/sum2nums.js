Blockly.Blocks['sum2nums'] = {
  init: function() {
	this.setColour(160);
    this.appendDummyInput()
        .appendField("sum");
    this.appendValueInput("n1");
    this.appendDummyInput()
        .appendField("+");
    this.appendValueInput("n2");
    this.setInputsInline(true);
	this.setOutput(true);
    //this.setPreviousStatement(true);
    //this.setNextStatement(true);
    this.setTooltip('');
  }
};

Blockly.JavaScript['sum2nums'] = function(block) {
  var value_n1 = Blockly.JavaScript.valueToCode(block, 'n1', Blockly.JavaScript.ORDER_ATOMIC);
  var value_n2 = Blockly.JavaScript.valueToCode(block, 'n2', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'game.sum2nums('+value_n1+','+value_n2+')';
  return [code, Blockly.JavaScript.ORDER_ADDITION];
};
