Blockly.Blocks['math_num_30'] = {
  init: function() {
    this.setColour(160);
	this.setOutput(true, 'Number');
	this.appendDummyInput()
		.appendField("30");
    this.setTooltip('');
  }
};

Blockly.JavaScript['math_num_30'] = function(block) {
  var code = 'game.useNum(30)';
  return [code, Blockly.JavaScript.ORDER_ADDITION];
};
