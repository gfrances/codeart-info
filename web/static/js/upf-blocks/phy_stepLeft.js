Blockly.Blocks['phy_stepLeft'] = {
     init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(270);
    this.appendValueInput("stepLeft")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("step left");
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('');
  }
};

Blockly.JavaScript['phy_stepLeft'] = function(block) {
   var value_goright = Blockly.JavaScript.valueToCode(block, 'stepLeft', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'this.stepLeft();';
  return code;
};
