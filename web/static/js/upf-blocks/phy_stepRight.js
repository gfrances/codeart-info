Blockly.Blocks['phy_stepRight'] = {
     init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(270);
    this.appendValueInput("stepRight")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("step right");
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('');
  }
};

Blockly.JavaScript['phy_stepRight'] = function(block) {
   var value_goright = Blockly.JavaScript.valueToCode(block, 'stepRight', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'this.stepRight();';
  return code;
};
