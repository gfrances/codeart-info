Blockly.Blocks['addNumber'] = {
  init: function() {
    this.setHelpUrl('http://www.w3schools.com/jsref/jsref_length_string.asp');
    this.setColour(160);
    this.appendDummyInput()		
		.appendField('addNumber')
        .appendField(new Blockly.FieldDropdown([ ["1", "1"], ["2", "2"], ["3", "3"], ["4", "4"], ["5", "5"], ["6", "6"], ["7", "7"], ["8", "8"], ["9", "9"]]), "Number");
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('');
  }
};

Blockly.JavaScript['addNumber'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var dropdown_number = block.getFieldValue('Number');
  // TODO: Assemble JavaScript into code variable.
  var code = 'game.addNumber('+dropdown_number+');';
  return code;
};