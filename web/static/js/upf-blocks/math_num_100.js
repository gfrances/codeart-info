Blockly.Blocks['math_num_100'] = {
  init: function() {
    this.setColour(160);
	this.setOutput(true, 'Number');
	this.appendDummyInput()
		.appendField("100");
    this.setTooltip('');
  }
};

Blockly.JavaScript['math_num_100'] = function(block) {
  var code = 'game.useNum(100)';
  return [code, Blockly.JavaScript.ORDER_ADDITION];
};
