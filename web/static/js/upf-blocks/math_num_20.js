Blockly.Blocks['math_num_20'] = {
  init: function() {
    this.setColour(160);
	this.setOutput(true, 'Number');
	this.appendDummyInput()
		.appendField("20");
    this.setTooltip('');
  }
};

Blockly.JavaScript['math_num_20'] = function(block) {
  var code = 'game.useNum(20)';
  return [code, Blockly.JavaScript.ORDER_ADDITION];
};
