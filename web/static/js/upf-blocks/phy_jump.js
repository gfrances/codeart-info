Blockly.Blocks['phy_jump'] = {
     init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(270);
    this.appendValueInput("stepRight")
        .setCheck("Number")
        .setAlign(Blockly.ALIGN_RIGHT)
        .appendField("jump");
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('');
  }
};

Blockly.JavaScript['phy_jump'] = function(block) {
   var value_goright = Blockly.JavaScript.valueToCode(block, 'jump', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'this.jumpVertical();';
  return code;
};
