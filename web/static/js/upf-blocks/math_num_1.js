Blockly.Blocks['math_num_1'] = {
  init: function() {
    this.setColour(160);
	this.setOutput(true, 'Number');
	this.appendDummyInput()
		.appendField("1");
    this.setTooltip('');
  }
};

Blockly.JavaScript['math_num_1'] = function(block) {
  var code = 'game.useNum(1)';
  return [code, Blockly.JavaScript.ORDER_ADDITION];
};
