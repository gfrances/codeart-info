Blockly.Blocks['math_num_10'] = {
  init: function() {
    this.setColour(160);
	this.setOutput(true, 'Number');
	this.appendDummyInput()
		.appendField("10");
    this.setTooltip('');
  }
};

Blockly.JavaScript['math_num_10'] = function(block) {
  var code = 'game.useNum(10)';
  return [code, Blockly.JavaScript.ORDER_ADDITION];
};
