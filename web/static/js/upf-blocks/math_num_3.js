Blockly.Blocks['math_num_3'] = {
  init: function() {
    this.setColour(160);
	this.setOutput(true, 'Number');
	this.appendDummyInput()
		.appendField("3");
    this.setTooltip('');
  }
};

Blockly.JavaScript['math_num_3'] = function(block) {
  var code = 'game.useNum(3)';
  return [code, Blockly.JavaScript.ORDER_ADDITION];
};
