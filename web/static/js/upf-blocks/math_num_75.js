Blockly.Blocks['math_num_75'] = {
  init: function() {
    this.setColour(160);
	this.setOutput(true, 'Number');
	this.appendDummyInput()
		.appendField("75");
    this.setTooltip('');
  }
};

Blockly.JavaScript['math_num_75'] = function(block) {
  var code = 'game.useNum(75)';
  return [code, Blockly.JavaScript.ORDER_ADDITION];
};
