
jQuery(function($){
    /* Sidebar toggling */
    $("#sidebar-toggle .menu-toggle").click(function (e) {
        e.preventDefault();
        $(this).find('.glyphicon').toggleClass('glyphicon-menu-left').toggleClass('glyphicon-menu-right')
        $("#wrapper").toggleClass("toggled");
    });
});