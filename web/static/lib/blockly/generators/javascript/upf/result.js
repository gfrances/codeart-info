Blockly.JavaScript['result'] = function(block) {
  var result_val = Blockly.JavaScript.valueToCode(block, 'result_val', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'game.result('+result_val+')';
  return code;
};
