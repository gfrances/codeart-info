Blockly.JavaScript['addNumber'] = function(block) {
  var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
  var dropdown_number = block.getFieldValue('Number');
  // TODO: Assemble JavaScript into code variable.
  var code = 'game.addNumber('+dropdown_number+');';
  return code;
};
