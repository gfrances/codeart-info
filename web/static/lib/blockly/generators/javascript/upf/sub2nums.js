Blockly.JavaScript['sub2nums'] = function(block) {
  var value_n1 = Blockly.JavaScript.valueToCode(block, 'n1', Blockly.JavaScript.ORDER_ATOMIC);
  var value_n2 = Blockly.JavaScript.valueToCode(block, 'n2', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = 'game.sub2nums('+value_n1+','+value_n2+')';
  return [code, Blockly.JavaScript.ORDER_ADDITION];
};
