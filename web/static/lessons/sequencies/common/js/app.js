'use strict';
/* App Controllers */

var gateGameApp = angular.module('gateGameApp', []);

gateGameApp.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

gateGameApp.factory('game', function(){
	var tileNames = ['gate1o'];
	return new Game(tileNames);
});

gateGameApp.controller('GameCtrl', function GameCtrl($scope, game){
	$scope.game = game;
});
