'use strict';
/* Memory Game Models and Business Logic */

function Tile(title) {
  this.title = title;
  this.flipped = false;
}

Tile.prototype.flip = function() {
	this.flipped = !this.flipped;
	if (this.flipped){ this.title = "gate1o.jpg"; this.finished = true;}
	else this.title = "gate1.jpg";
}

function Game(tileNames) {
	this.gate = new Tile("gate1.jpg");
	this.finished = false;

	this.flipTile = function(){
		if(!this.finished)
			this.gate.flip();
	}

    this.resetVars = function(){
        this.gate = new Tile("gate1.jpg");
        this.finished = false;
        this.title = "gate1.jpg";
		document.getElementById("result").innerHTML = 'Result: ?';
    }

	this.useNum = function(a){
		return a;
	}

	this.sum2nums = function(a , b){
		return a+b;
	}
	
	this.div2nums = function(a , b){
		return a/b;	
	}
	
	this.sub2nums = function(a , b){
		return a-b;	
	}
		
	this.mul2nums = function(a , b){
		return a*b;
	}
	
	this.result = function(r){
		if(r == 213){
			this.flipTile();	
			this.finished = true;
			document.getElementById("result").innerHTML = 'Result: <font color="green">213<font>';
		}
		else{
			document.getElementById("result").innerHTML = 'Result: <font color="red">'+r+'<font>';
		}
	}
	
}

