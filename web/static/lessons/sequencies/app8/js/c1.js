jQuery(function($){
    var init = function() {
        Blockly.inject(document.body, {maxBlocks: 12, toolbox: document.getElementById('toolbox'), scrollbars: true, trashcan: true});
        // Let the top-level application know that Blockly is ready.
        window.parent.upf_app.blocklyLoaded(Blockly);
    }


    function myUpdateFunction() {
		parent.document.getElementById('capacity').innerHTML = Blockly.mainWorkspace.remainingCapacity();
    }

    init();

    Blockly.addChangeListener(myUpdateFunction);
});
