'use strict';
/* Memory Game Models and Business Logic */


function Tile(title) {
  this.title = title;
  this.flipped = false;
}

Tile.prototype.flip = function() {
	this.flipped = !this.flipped;
	if (this.flipped){ this.title = "gate1o.jpg"; this.finished = true;}
	else this.title = "gate1.jpg";
}

function Game(tileNames) {
	this.gate = new Tile("gate1.jpg");
	this.number = 0;
	this.totalDigits = 0;
	this.finished = false;
	this.textapp = "?3 + 1? = 59";

	this.flipTile = function(){
		this.gate.flip();
	}

    this.resetVars = function(){
        this.gate = new Tile("gate1.jpg");
        this.number = 0;
        this.totalDigits = 0;
        this.finished = false;
        this.title = "gate1.jpg";
		this.textapp = "?3 + 1? = 59";
		document.getElementById("textapp").innerHTML = this.textapp;
    }

	this.addNumber = function(numb){
		if(this.finished) return false;
		this.totalDigits = this.totalDigits + 1;
		this.number = this.number*10 + numb;
		if(this.totalDigits == 1){
			this.textapp = this.textapp.replace("?",numb);
			document.getElementById("textapp").innerHTML = this.textapp;
		}
		else if(this.totalDigits == 2){
			this.textapp = this.textapp.replace("?",numb);
			if(this.number == 46){
				this.flipTile();
				this.textapp = '<font color="green">'+this.textapp+'<font>';
				document.getElementById("textapp").innerHTML = this.textapp;
				return true;
			}
			this.textapp = '<font color="red">'+this.textapp+'<font>';
			document.getElementById("textapp").innerHTML = this.textapp;
			this.number = 0;
			this.totalDigits = 0;
		}

		return false;
	}
	
}

