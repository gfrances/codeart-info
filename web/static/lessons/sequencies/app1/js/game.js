'use strict';
/* Memory Game Models and Business Logic */

function Tile(title) {
  this.title = title;
  this.flipped = false;
}

Tile.prototype.flip = function() {
	this.flipped = !this.flipped;
	if (this.flipped){ 
		this.title = "gate1o.jpg"; 
		this.finished = true;
	}
	else this.title = "gate1.jpg";
}

function Game(tileNames) {
	this.gate = new Tile("gate1.jpg");
	this.number = 0;
	this.totalDigits = 0;
	this.finished = false;

	this.flipTile = function(){
		this.gate.flip();
	}

    this.resetVars = function(){
        this.gate = new Tile("gate1.jpg");
        this.number = 0;
        this.totalDigits = 0;
        this.finished = false;
        this.title = "gate1.jpg";
		document.getElementById("textapp").innerHTML = '1962';
    }

	this.addNumber = function(numb){
		if(this.finished) return false;
		this.totalDigits = this.totalDigits + 1;
		this.entered = this.entered+1;
		this.number = this.number*10 + numb;
		if(this.totalDigits == 1){
			if(numb == 1) 
				document.getElementById("textapp").innerHTML = '<font color="green">1<font>';
			else
				document.getElementById("textapp").innerHTML = '<font color="red">'+numb+'<font>';
				
		}
		else if(this.totalDigits == 2){
			if(numb == 9) 
				document.getElementById("textapp").innerHTML += '<font color="green">9<font>';
			else
				document.getElementById("textapp").innerHTML += '<font color="red">'+numb+'<font>';
		}
		else if(this.totalDigits == 3){
			if(numb == 6) 
				document.getElementById("textapp").innerHTML += '<font color="green">6<font>';
			else
				document.getElementById("textapp").innerHTML += '<font color="red">'+numb+'<font>';
		}
		else if(this.totalDigits == 4){
			if(numb == 2) 
				document.getElementById("textapp").innerHTML += '<font color="green">2<font>';
			else
				document.getElementById("textapp").innerHTML += '<font color="red">'+numb+'<font>';
			if(this.number == 1962){
				this.flipTile();
				return true;
			}
			this.number = 0;
			this.totalDigits = 0;
		}
		return false;
	}
	
}

