jQuery(function($){
    var init = function() {
        Blockly.inject(document.body, {maxBlocks: 2, toolbox: document.getElementById('toolbox')});
        // Let the top-level application know that Blockly is ready.
        window.parent.upf_app.blocklyLoaded(Blockly);
    }


    function myUpdateFunction() {
		parent.document.getElementById('capacity').innerHTML = Blockly.mainWorkspace.remainingCapacity();
    }

    init();

    Blockly.addChangeListener(myUpdateFunction);
});
