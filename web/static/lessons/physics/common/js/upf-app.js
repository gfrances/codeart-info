

window.upf_app = {
    blocklyLoaded: function(blockly)
    {
        this.blockly = blockly;
    }};

jQuery(function($){

    var $canvas = $('.canvas-placeholder');
    var $display = $('.code-display');
    var appElement =  document.querySelector('[ng-app=gateGameApp]');
    var appScope = angular.element(appElement).scope();
    var controllerScope = appScope.$$childHead;
    var game = controllerScope.game;

    var canvas_target = $canvas.data('target');

    $('<iframe class="upf-canvas" src="' + canvas_target +  '" frameBorder="0" seamless="seamless"></iframe>')
        .width($canvas.width())
        .appendTo($canvas);

    $('#run-code').click(function(){
        var code = window.upf_app.blockly.JavaScript.workspaceToCode();

        //prepare game var on which the js functions will be evaluated
        
        game.resetVars();
        controllerScope.$apply();

        eval(code);

        //apply all the code on the angularjs controller
        controllerScope.$apply();
    });

    $('#clear-code').click(function(){
        window.upf_app.blockly.mainWorkspace.clear();
        controllerScope.game.resetVars();
        controllerScope.$apply();
    })

});
