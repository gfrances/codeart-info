function Game(){
    var canvas, ctx, w, h, zoom=30, jumpSpeed=6, walkSpeed=30,
    world, characterBody, planeBody, platforms=[], boxes=[];
    var goright = true;
    var code;
    var stepnum;
    var runAnimation = {
        value: false
    };

this.setCode = function (code) {
    this.code = code;
}

this.clearCanvas = function(){
    if(runAnimation.value)
        this.startstop();
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.canvas.width = this.canvas.width;
}

this.init = function(){
    // Init canvas
    this.stepnum = 0;
    this.canvas = document.getElementById("myCanvas");
    label = document.getElementById("label");

    w = this.canvas.width;
    h = this.canvas.height;
    this.ctx = this.canvas.getContext("2d");
    this.clearCanvas();
    this.ctx.lineWidth = 1/zoom;

    // Init world
    world = new p2.World();

    world.defaultContactMaterial.friction = 0.5;
    world.setGlobalStiffness(1e5);
    world.islandSplit = true;
    world.sleepMode = p2.World.ISLAND_SLEEPING;
    // Init materials
    var groundMaterial = new p2.Material(),
        characterMaterial = new p2.Material(),
        boxMaterial = new p2.Material();

    // Add a character body
    characterShape = new p2.Rectangle(0.5,1);
    characterBody = new p2.Body({
        mass: 1,
        position:[7,-0.5],
        fixedRotation: true
    });
    var characterID = characterBody.id;
    characterBody.addShape(characterShape);
    world.addBody(characterBody);
    characterShape.material = characterMaterial;
    characterBody.damping = 0.5;

    // Add a ground plane
    planeShape = new p2.Plane();
    planeBody = new p2.Body({
        position:[0,-1]
    });
    planeBody.addShape(planeShape);
    world.addBody(planeBody);
    planeShape.material = groundMaterial;

    // Add platforms
    //var platformPositions = [[2,0],[0,1],[-2,2],[-4,3], [-5,4]],
    var platformPositions = [],
        platformShape = new p2.Rectangle(1,0.3);
    for(var i=0; i<platformPositions.length; i++){
        var platformBody = new p2.Body({
            mass: 0, // Static
            position:platformPositions[i]
        });
        platformBody.sensor = true;
        platformBody.type = p2.Body.KINEMATIC;
        platformBody.addShape(platformShape);
        world.addBody(platformBody);
        platforms.push(platformBody);
    }
    platformShape.material = groundMaterial;

    // Add movable boxes
    var boxPositions = [[2,1],[0,2],[-2,3],[-4,4]],
        boxShape = new p2.Rectangle(0.8,0.8);
    for(var i=0; i<boxPositions.length; i++){
        var boxBody = new p2.Body({
            mass: 1,
            position:boxPositions[i]
        });
        boxBody.addShape(boxShape);
        world.addBody(boxBody);
        boxes.push(boxBody);
    }
    boxShape.material = boxMaterial;

    // Init contactmaterials
    var groundCharacterCM = new p2.ContactMaterial(groundMaterial, characterMaterial,{
        friction : 0.0 // No friction between character and ground
    });
    var boxCharacterCM = new p2.ContactMaterial(boxMaterial, characterMaterial,{
        friction : 0.0 // No friction between character and boxes
    });
    var boxGroundCM = new p2.ContactMaterial(boxMaterial, groundMaterial,{
        friction : 0.6 // Between boxes and ground
    });
    world.addContactMaterial(groundCharacterCM);
    world.addContactMaterial(boxCharacterCM);
    world.addContactMaterial(boxGroundCM);
    world.on("endContact",function(evt){
        this.ctx.fillStyle = 'green';
    });
}

this.drawBox = function(body){
    this.ctx.beginPath();
    var x = body.position[0],
        y = body.position[1],
        s = body.shapes[0];
    this.ctx.save();
    this.ctx.translate(x, y);     // Translate to the center of the box
    this.ctx.rotate(body.angle);  // Rotate to the box body frame
    this.ctx.fillRect(-s.width/2, -s.height/2, s.width, s.height);
    this.ctx.restore();
}

this.drawPlane = function(){
    var y1 = planeBody.position[1],
        y0 = -h/zoom/2,
        x0 = -w/zoom/2,
        x1 = w/zoom/2;
    this.ctx.fillRect(x0, y0, x1-x0, y1-y0);
}

this.render = function(){
    // Clear the canvas
    this.ctx.clearRect(0,0,w,h);

    // Transform the canvas
    // Note that we need to flip the y axis since Canvas pixel coordinates
    // goes from top to bottom, while physics does the opposite.
    this.ctx.save();
    this.ctx.translate(w/2, h/2);  // Translate to the center
    this.ctx.scale(zoom, -zoom);   // Zoom in and flip y axis

    // Draw all bodies
    this.ctx.strokeStyle = 'none';

    this.ctx.fillStyle = 'green';
    this.drawPlane();
    for(var i=0; i<platforms.length; i++){
        this.drawBox(platforms[i]);
    }

    this.ctx.fillStyle='red';
    this.drawBox(characterBody);

    this.ctx.fillStyle='blue';
    for(var i=0; i<boxes.length; i++){
        this.drawBox(boxes[i]);
    }

    // Restore transform
    this.ctx.restore();
}

this.setPlatformsVelocity = function(velocity){
    for(var i=0; i<platforms.length; i++){
        platforms[i].velocity[0] = velocity;
    }

}
//start/stop animation
this.startstop=function(){
        runAnimation.value = !runAnimation.value;
        if(runAnimation.value) {
            this.animate();
        }
    }

// Animation loop
this.animate = function(){
    if(runAnimation.value) {
        requestAnimationFrame(this.animate.bind(this));
        //for(var i=0; i<platforms.length; i++){
        //    platforms[i].velocity[0] = Math.sin(t*0.001);
        //}
        this.doOneUpdate();
        // Move physics bodies forward in time
        world.step(1 / 60);
        // Render scene
        this.render();
        if (!this.checkIfCanGoRight()){
            alert("Good you solved this one!!!\n"+"The code you generated is:\n"+this.code);
            this.startstop();
        }
    }
}

this.stepRight = function(){
    this.walkRight();
}
this.stepLeft = function(){
     this.walkLeft();
}

this.walkRight=function(){
    characterBody.velocity[0] =  walkSpeed;
}

this.walkLeft=function(){
    characterBody.velocity[0] =  -walkSpeed;
}

this.stopWalking=function(){
    characterBody.velocity[0] = 0;
}

function getDistanceFromFirstPlatform(){
    return characterBody.position[0] - platforms[0].position[0];
}

function isNearFirstPlatform(){
    return getDistanceFromFirstPlatform() < 1;
}

function isOnFirstPlatform(){
    return (characterBody.position[1] == platforms[0].position[1]-1);
}

function goToFirstPlatform(){
    var dist = getDistanceFromFirstPlatform();
    if(dist > 0) {
        if(isNearFirstPlatform())
            jumpLeft();
        else
            walkLeft();
    }
    else if(isOnFirstPlatform())
        stopWalking();
    else {
        if(isNearFirstPlatform())
            jumpRight();
        else
            walkRight();
    }
}

function isOnFirstPlatform(){
    return Math.abs(getDistanceFromFirstPlatform()) < 0.1
}

function isNearFirstPlatform(){
    return Math.abs(getDistanceFromFirstPlatform()) < 0.75;
}

function jumpVertical(){
    if(checkIfCanJump()) characterBody.velocity[1] = jumpSpeed;
}

function jumpLeft(){
    if(checkIfCanJump()) characterBody.velocity[1] = jumpSpeed;
    walkLeft();
}

function jumpRight(){
    if(checkIfCanJump()) characterBody.velocity[1] = jumpSpeed;
    walkRight();
}


this.doOneUpdate = function(){
    var instrs = this.code.split(';');
    if(this.stepnum == instrs.length){
        this.stopWalking();
        return;
    }
    instrs[this.stepnum] = instrs[this.stepnum].replace('game','this');
    eval(instrs[this.stepnum]+';');
    this.stepnum++;
}

this.checkIfCanGoRight=function() {
    return (characterBody.position[0] < w / zoom / 2);
}

this.checkIfCanGoLeft=function(){
    return (characterBody.position[0]>-w/zoom/2);
}

var yAxis = p2.vec2.fromValues(0,1);
function checkIfCanJump(){
    var result = false;
    for(var i=0; i<world.narrowphase.contactEquations.length; i++){
        var c = world.narrowphase.contactEquations[i];
        if(c.bodyA === characterBody || c.bodyB === characterBody){
            var d = p2.vec2.dot(c.normalA, yAxis); // Normal dot Y-axis
            if(c.bodyA === characterBody) d *= -1;
            if(d > 0.5) result = true;
        }
    }
    return result;
}

}