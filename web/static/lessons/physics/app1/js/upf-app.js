

window.upf_app = {
    blocklyLoaded: function(blockly)
    {
        this.blockly = blockly;
    }};


jQuery(function($){
    $canvas = $('.canvas-placeholder');
    $display = $('.code-display');
    var game = new Game();
    game.init();
    game.render();
    var canvas_target = $canvas.data('target');

     $('<iframe class="upf-canvas" src="' + canvas_target +  '" frameBorder="0" seamless="seamless"></iframe>')
        .width($canvas.width())
        .appendTo($canvas);

    $('#run-code').click(function(){
        var code = window.upf_app.blockly.JavaScript.workspaceToCode();
        game.setCode(code);
        game.startstop();
    });

    $('#stop-code').click(function(){
        game.startstop();
    })

    $('#clear-code').click(function(){
        window.upf_app.blockly.mainWorkspace.clear();
        game.clearCanvas();
        game = new Game();
        game.init();
        game.render();
    })

});
