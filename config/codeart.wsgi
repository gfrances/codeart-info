#!/usr/bin/python
import os
import sys
import logging
logging.basicConfig(stream=sys.stderr)

# Activate the virtual environment
basedir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
activate_this = basedir + '/venv/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

# Import and load the application itself.
sys.path.insert(0, basedir)
from web import app as application
application.secret_key = 'sdfl127d387xcvbkDSAhjasdbSAD98y'

